# $Id: Makefile,v 1.23 2003/11/14 22:32:13 aventimiglia Exp $
#
# CLHP the Common Lisp Hypertext Preprocessor
# (C) 2003 Anthony J Ventimiglia
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# email: aventimiglia@common-lisp.net
# HomePage: http://common-lisp.net/project/clhp/

# This is where lisp sources will be installed
SRCDIR=/usr/lib/cmucl/source/clhp

# Directory to install the clhp lisp core
COREDIR=/usr/lib/cmucl

# Directory to install documentation
DOCDIR=/usr/local/share/clhp

# path to cmucl
LISP=/usr/bin/lisp

# path to install binary
INSTALL=/bin/install

# path to chmod
CHMOD=/bin/chmod

# path to tar
TAR=/bin/tar

# path to apxs
APXS=/usr/sbin/apxs

DIRMODE=0775 	# mode for cmucl dirs
FMODE=0664	# mode for installed files

############### DO NOT EDIT ANYTHING BELOW THIS LINE ######################

INSTALLDIR=$(INSTALL) -m $(DIRMODE) -d
INSTALLFILE=$(INSTALL) -m $(FMODE)

LISPC=$(LISP) -noinit -load config/lispc -eval '(make)'

BUILD_CORE=config/build-core.x86f
APACHE_MOD=mod_clhp.so
SOURCES=cgi.lisp clhp.lisp package.lisp clhp.asd mod_clhp.c utility.lisp
CORE=clhp.core
LISPCBIN=config/lispc.x86f
DOCS=COPYING INSTALL ChangeLog NEWS README TODO 

DIST_SUBDIRS=config examples
NON_DIST_SUBDIRS=tests
RECURSIVE_SUBDIRS = tests
EXTRADIST=config/build-core.lisp config/lispc.lisp examples/index.clhp \
	Makefile config/lisp-init.lisp

NON_DIST= tests/Makefile tests/cgi-test.lisp tests/test-suite.lisp

CLEAN_TARGETS=$(LISPCBIN) $(CORE) $(CORE).BAK $(TARBALL) \
		 $(APACHE_MOD) mod_clhp.o

DISTFILES=$(SOURCES) $(DOCS) $(EXTRADIST)

# This is the version for the next release, during cvs development the
# real version number is based on the date of the last change to
# clhp.lisp, the only place this version carries in is during
# make-dist.
#
# I've put a nice little trick to make sure tarballs made with make
# dist have a cvs-type versioning number. The version here in the
# makefile most likeley will not match the cvs version ID in
# clhp.lisp, since they depend on their own file's cvs date keyword.

CVSID=$(shell echo $Date: 2003/11/14 22:32:13 $ | \
	awk '{print $$2}'| sed s/\\///g )cvs

VERSION=$(CVSID)
#VERSION=0.2.1alpha
TARROOT=clhp-$(VERSION)
TARBALL=$(TARROOT).tar.gz

RECURSIVE_TARGETS=clean-recursive clean-all-recursive check-recursive

all: $(LISPCBIN) $(CORE)

module:$(APACHE_MOD)

$(APACHE_MOD): mod_clhp.c
	$(APXS) -c $<

.PHONY: all install uninstall clean clean-all clean-backups install-dirs \
	install-source install-source-dirs uninstall-files uninstall-dirs \
	install-core clean-dist $(RECURSIVE_TARGETS) check \
	 module-install

%.x86f: %.lisp
	$(LISPC) $<

%.clcgi: %.x86f
	mv -v $< $@

check: $(CORE) check-recursive

$(LISPCBIN): config/lispc.lisp

$(CORE): $(SOURCES) $(BUILD_CORE)
	$(LISP) -noinit -load "config/build-core"

module-install: $(APACHE_MOD)
	$(APXS) -i -a $<

install: all install-dirs install-core
	$(INSTALL) $(DOCS) $(DOCDIR)

install-core: $(CORE) install-dirs
	$(INSTALLFILE) -b -S .BAK $(CORE) $(COREDIR)

install-source: install install-source-dirs
	for f in $(SOURCES); do \
		$(INSTALLFILE) $$f $(SRCDIR) ; \
	done

install-dirs:
	$(INSTALLDIR) $(COREDIR)
	$(INSTALLDIR) $(DOCDIR)

install-source-dirs: 
	$(INSTALLDIR) $(SRCDIR)

uninstall: uninstall-files uninstall-dirs

uninstall-files:
	for f in $(SOURCES) ; do \
		if test -f $(SRCDIR)/$$f; then rm -v $(SRCDIR)/$$f ; fi ; \
	done
	for f in $(DOCS) ; do \
		fullpath=$(DOCDIR)/$$f ; \
		if test -f $$fullpath; then rm -v $$fullpath ; fi ; \
	done
	core=$(COREDIR)/$(CORE) ; \
	if test -f $$core ; then rm -v $$core; fi ; \
	if test -f $$core.BAK; then mv -v $$core.BAK $$core; fi

uninstall-dirs: uninstall-files
	-rmdir -v $(SRCDIR)
	-rmdir -v $(COREDIR)
	-rmdir -v $(BINDIR)
	-rmdir -v $(DOCDIR)

dist: $(TARBALL) clean-dist

$(TARBALL): $(DISTFILES)
	distdir=clhp-$(VERSION) ; \
	if test ! -d $$distdir; then mkdir -v $$distdir ; fi ; \
	for dir in $(DIST_SUBDIRS); do \
		if test ! -d $$distdir/$$dir; then \
			 mkdir -v $$distdir/$$dir ; \
		fi ; \
	done ; \
	for file in $(DISTFILES); do \
		cp -v $$file $$distdir/$$file ; \
	done 
	tarfile=$(TARROOT).tar ; \
	if test -f $$tarfile.gz; then rm -v $$tarfile.gz; fi ; \
	tar cvf $$tarfile $(TARROOT) ; \
	gzip $$tarfile

clean-all: clean-all-recursive clean clean-backups clean-dist 

clean: clean-recursive
	for f in $(CLEAN_TARGETS); \
	 do \
		if test -f $$f ; then rm -v $$f ; fi \
	done
	find -iname "*.x86f" -exec rm -v {} \;
	find -iname "*.tar.gz" -exec rm -v {} \;

clean-backups: 
	find -iname "*~" -exec rm -v {} \;

clean-dist:
	if test -d $(TARROOT); then rm -rv $(TARROOT); fi

$(RECURSIVE_TARGETS):
	@set fnord $(MAKEFLAGS); amf=$$2 \
	dot_seen=no ; \
	target=`echo $@ | sed s/-recursive//`; \
	list='$(RECURSIVE_SUBDIRS)' ; for subdir in $$list; do \
		echo ">>>>> Making $$target in $$subdir"; \
		(cd $$subdir && $(MAKE) $$target) || \
		case "$$amf" in 		\
			*=*) exit 1 ;;		\
			*k*) exit 1 ;;		\
			*) exit 1 ;;		\
		esac ;				\
	done