(ext:file-comment
 "$Id: lispc.lisp,v 1.5 2003/11/10 15:39:04 aventimiglia Exp $")
;;
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/
(eval-when (:compile-toplevel)
  (load "config/lisp-init"))

;; This isn't really necessary, but it creates an exit code if there
;; is a compile-time error, making make aware of the problem, and not
;; dropping into the debugger
(defun make ()
  (let ((filename (car (last ext:*command-line-strings*))))
    (format t "; LISPC -- compiling ~a~%" filename)
    (handler-case
     (compile-file filename)
     (error (c) 
	    (format t
		    "; LISPC -- ~A Compilation failed.~%"
		    (type-of c))
	    (format t "I'm sorry that this error report is so poor, compilation is being done via config/lispc.lisp. If you want to no what failed, you can try to compile interactively, or improve lispc.lisp to give more detailed error reports.~%")
	    (unix:unix-exit 1))))
  (unix:unix-exit 0))
		  
     