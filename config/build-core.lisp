(ext:file-comment
 "$Id: build-core.lisp,v 1.5 2003/11/10 15:37:04 aventimiglia Exp $")
;;
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/


;; this is an installation program that will load the current lisp core,
;; load the new cgi library, and then dump the core locally.
;;
;; I guess by doing this, I don't need to actually save the cgi.x86f binary
;; in the lisp directory, but I'll do it anyway so it is available for
;; someone else making a custom core.
;;
;; This is a somewhat modified version of the config.lisp program distributed
;; with cmucl. config.lisp could have been used, but this is automatic and
;; not subject to change

(eval-when (:load-toplevel :compile-toplevel)
  (load "config/lisp-init"))

(in-package "USER")

(block abort
  (let ((output-file #p"clhp.core")
;	(packages '(:cgi :clhp)))
)
	(gc-off)

    (asdf:operate 'asdf:load-op 'clhp)
    
;    (dolist (package packages)
;      (when (find-package package)
;        (delete-package package))
;      (handler-case
;       (load (format nil "~A" (string-downcase (string package))))
;       (file-error (c)
;                   (format
;                    t
;                    "There was an file-error in loading \"~A\"~%Perhaps the proper packages did not build successfully, or maybe you're mokeying around.~%"
;                    (file-error-pathname c))
;                   (unix:unix-exit 1))))
;
    (when (probe-file output-file)
      (multiple-value-bind
	  (ignore old new)
	  (rename-file output-file
		       (concatenate 'string (namestring output-file)
				    ".BAK"))
	(declare (ignore ignore))
	(format t "~&Saved ~S as ~S.~%" (namestring old) (namestring new))))
    (setf lisp::*need-to-collect-garbage* nil)
    (gc-on)
    (save-lisp output-file)))

(unix:unix-exit 0)

