#+cmu (ext:file-comment "$Id: cgi.lisp,v 1.14 2003/10/21 16:57:27 aventimiglia Exp $")
;;
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/

(in-package :clhp)

(defmacro debug (expression)
  "Print out EXPRESSION and the result of (EVAL EXPRESSION)"
  `(format t "(CLHP:DEBUG: ~s --> ~s)~%" ,expression (eval ,expression)))

(defmacro explode-string (string)
  "Converts a string to a list of chars, this is an aux function used
for string processing.
ex: (EXPLODE-STRING (\"Hello\") --> (#\H #\e #\l #\l #\o)"
  `(concatenate 'list ,string))

;; External Symbol section

(defvar *server-env* (make-hash-table)
  "This is a hash-table variables passed by the thw key is a keyword
and all values are stored as strings.")

(defvar *query-vars* (make-hash-table :test 'equal)
  "A hash-table of all variables passed through a GET or POST method, the
key is a string, and all values are stored in string form.")

;; This sets the main variables, since the library is already part of the lisp
;; core, we can't use an eval-when, I may eventually make a cgi:init that also
;; prints the header. 
(defun init ()
  "Initialize CGI, this should be called before any globals are
accessed"
  (mapcar #'(lambda (key/val)
	      (setf (gethash (car key/val) *server-env*)
		    (cdr key/val)))
	  *environment-list*)
  (mapcar #'(lambda (key/val-list)
	      (setf (gethash (car key/val-list) *query-vars*)
		    (cadr key/val-list)))
	  (cond-bind
	   ((request-method (make-keyword
			     (gethash :REQUEST_METHOD *server-env*))))
	   ((eql request-method :POST)
	    (query-to-a-list (post-data)))
	   ((eql request-method :GET)
	    (query-to-a-list (get-data)))))
  (values))

;;
;; End of external symbols
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Internal symbols follow
;;


;; I know I could have used a regexp library for the string functions, but
;; That would make a dependency and then I'd have to also install that
;; regex lib in the core. I don't need a whole lot of string functions
;; yet, so for now it's simple enough to write the stuff I need


;; ex:
;; (mapcar #'implode-string
;; 	(split-char-list #\Space
;; 			 (explode-string "In God We Trust" ))) -->
;; ("In" "God" "We" "Trust")  
(defun split-char-list (char char-list)
  "Splits a char-list (EXPLODEd string) on CHAR."
  (labels
      ((split
	(char-list split-list)
	(if-bind ((position (position char char-list)))
	   (null position)
	      (remove nil (nreverse (cons char-list split-list)))
	    (split (nthcdr (1+ position) char-list)
		   (cons (butlast char-list (- (length char-list) position))
			 split-list)))))
    (split char-list nil)))

;; !!!!!!!!! This should most likely be tested and improved , because
;; if the CGI program is given a bogus Content-Length header, this
;; will choke -- In other words, it's prone to attacks
(defun read-n-chars (count &optional (stream *standard-input*))
    "Reads N chars from STREAM, returning a list of chars. Be careful,
there is no check for EOF. This is specifically designed for POST
reading, where the exact amount of input data is known."
    (labels
	((rec
	  (count char-list)
	  (if (zerop count)
	      (nreverse char-list)
	    (rec (1- count) (cons (read-char stream) char-list)))))
      (rec count nil)))

(defun get-data ()
  "Returns GET data (QUERY_STRING) as an exploded string"
  (explode-string (gethash :QUERY_STRING *server-env*)))

;; The closure makes sure we don't try to read from stdin twice
(let ((get-switch nil)
      (post-char-list nil))  
      (defun post-data ()
	"Returns POST data as an exploded string"
	(if (not get-switch)
	    (progn
	      (setf
	       get-switch t
	       post-char-list (read-n-chars
			       (read-from-string
				(gethash :CONTENT_LENGTH *server-env*))))
	      post-char-list)
	  post-char-list)))

(defun query-to-a-list (get/post-data)
  (list-to-a-list
   (mapcar #'implode-string
	   (mapcan #'(lambda (c) (split-char-list #\= c))
		   (split-char-list
		    #\& (url-decode-char-list get/post-data))))))

(defun url-decode-char-list (char-list)
  "Decodes encoded URL chars as per RFC 1738"
  (handler-bind
   ((general-error #'handle-general-error))
   (labels
       ((decode-error
	 ()
	 (signal 'general-error
		 :message (format nil
				  "~S is a malformed URL encoded string."
			 	  (implode-string char-list))))	 
	(decode-next
	 (encoded-part &optional decoded-part)
	 (cond-bind ((front-char (car encoded-part)))
	    ((null encoded-part) (nreverse decoded-part))
	    ((char= #\% front-char)
	     (if (<= 3 (length encoded-part))
		 (decode-next (cdddr encoded-part)
			      (cons (decode-char
				     (subseq encoded-part 1 3))
				    decoded-part))
	       (decode-error)))
	    ((char= #\+ front-char)
	     (decode-next (cdr encoded-part)
			  (cons #\Space decoded-part)))
	    (t (decode-next (cdr encoded-part)
			    (cons front-char decoded-part)))))
	(hex2dec (string-num)
		 (setf *read-base* 16)
		 (prog1
		     (read-from-string string-num)
		   (setf *read-base* 10)))
	(decode-char (char-code-list)
		     (if-bind ((great (car char-code-list))
			       (least (cadr char-code-list)))
			(and (digit-char-p great 16)
			     (digit-char-p least 16))
			(code-char (hex2dec
				    (format nil "~a~a" great least)))
			(decode-error))))
     (decode-next char-list))))

(defun implode-string (char-list)
  "Converts EXPLODEd CHAR-LIST into string, used as an aux function
for string processing.
ex: (IMPLODE-STRING '(#\H #\e #\l #\l #\o)) --> \"Hello\"
    (IMPLODE-STRING (EXPLODE-STRING \"Hello\")) --> \"Hello\""
  (coerce char-list 'string))

(define-condition general-error (error)
  ((message :initarg :message
	    :reader error-message))
  (:documentation "This is a top level error condition for the CGI Package."))

(defun handle-general-error (condition)
  "A generic error handler function, this will basicall just print a
text header, echoing the error-message."
  (format t "CL-CGI: ~A~%" (error-message condition))
  (quit))

