#+cmu (ext:file-comment "$Id: clhp.asd,v 1.5 2003/11/14 22:32:10 aventimiglia Exp $")
;; 
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/

#-cmu (warn "The CLHP Package has only been developed for CMUCL please feel free to port")

#-asdf (error "You need ASDF to build this")

(in-package :asdf)

(defsystem #:clhp
  :name "net.common-lisp.aventimiglia.clhp"
  :author "Anthony Ventimiglia <anthony@ventimiglia.org>"
  :maintainer "Anthony Ventimiglia <anthony@ventimiglia.org>"
  :version  #.(or nil
		  (let* ((trimmed (string-trim "$ Date:" "$Date: 2003/11/14 22:32:10 $"))
			 (date (subseq trimmed 0 (search " " trimmed))))
		    (concatenate 'string
				 (subseq date 0 4)
				 (subseq date 5 7)
				 (subseq date 8 10)
				 "cvs")))
  :description "CLHP the Common Lisp Hypertext Preprocesor"
  :long-description ""
  :licence "GNU Lesser General Public License"
  :components
  ((:file "package")
   (:file "utility" :depends-on ("package"))
   (:file "cgi" :depends-on ("utility"))
   (:file "clhp" :depends-on ("cgi"))))
