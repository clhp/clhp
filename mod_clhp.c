/* $Id: mod_clhp.c,v 1.4 2003/11/12 21:57:40 aventimiglia Exp $
   
  CLHP the Common Lisp Hypertext Preprocessor
  (C) 2003 Anthony J Ventimiglia
 
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
 
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
 
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
  email: aventimiglia@common-lisp.net
  HomePage: http://common-lisp.net/project/clhp/

  Apache module for clhp
 */

#include "httpd.h"
#include "http_config.h"
#include "http_protocol.h"
#include "util_script.h"
#include <string.h>

#ifndef HUGE_STRING_LEN
#define HUGE_STRING_LEN 65000
#endif

// AP_CALL_EXEC does not properly pass it's arguments
#define BROKEN_AP_CALL_EXEC 1

#define LISP_COMMAND "/usr/bin/lisp"
#define LISP_OPTIONS "-noinit -nositeinit -quiet -batch"
#define LISP_CORE "-core /usr/lib/cmucl/clhp.core"
#define LISP_EVAL "-eval '(clhp:parse (cdr  (assoc :script_filename ext:*environment-list*)))(quit)'"

typedef struct _child {
  request_rec *req;
} child_stuff;

/* Child init, right now this just runs a quick Lisp command */
static int run_lisp ( void *void_child_stuff, child_info *pinfo )
{
  child_stuff *child = (child_stuff *) void_child_stuff;
  char **env = ap_create_environment ( child->req->pool, 
				       child->req->subprocess_env );
  int child_pid;

#ifndef BROKEN_AP_CALL_EXEC
  child->req->args = ap_psprintf ( child->req->pool, "%s %s %s", LISP_OPTIONS, 
				   LISP_CORE, LISP_EVAL );
  ap_cleanup_for_exec ();
  child_pid = ap_call_exec ( child->req, pinfo, LISP_COMMAND, env, 1 );
#else
  char *command = ap_psprintf (child->req->pool, "%s %s %s %s", LISP_COMMAND, 
			       LISP_OPTIONS, LISP_CORE, LISP_EVAL );
  ap_cleanup_for_exec ();
  child_pid = ap_call_exec ( child->req, pinfo, command, env, 1 );
#endif    
  
#ifdef WIN32
  return ( child_pid );
#else
  exit ( 0 );
  // Should not get here
  return 0;
#endif
}

static int Handler ( request_rec *req )
{
  BUFF *lisp_out, *lisp_in;
  child_stuff child;

  ap_add_common_vars ( req );
  ap_add_cgi_vars ( req );
  child.req = req;

  // First we should check if the document exists
  // But for now...
  req->content_type = "text/html";
  ap_table_set ( req->headers_out, "X-Powered-By", 
		 "CLHP Common Lisp Hypertext Preprocessor" );
  ap_send_http_header ( req );

  if ( ! ap_bspawn_child ( req->pool, run_lisp, (void *) &child,
			   kill_always, &lisp_out, &lisp_in, NULL ))
    return HTTP_INTERNAL_SERVER_ERROR;

  // ap_hard_timeout ( "Hard Timeout", req );
  ap_bclose ( lisp_out );
  ap_send_fb ( lisp_in, req );
  ap_bclose ( lisp_in );
  ap_child_terminate ( req );

  return OK;
}


// Associate the handler with Handler String
static handler_rec Handlers[] =
  {
    { "clhp-handler", Handler },
    { NULL }
  };

module clhp_module = {
  STANDARD_MODULE_STUFF,
  NULL, 			/* Initializer */
  NULL,				/* dir_config creator */
  NULL,				/* dir merger */
  NULL,				/* server config */
  NULL,				/* merge server configs */
  NULL,				/* command-table */
  Handlers,			/* handlers */
  NULL,				/* Filename translation */
  NULL,				/* check user ID */
  NULL,				/* Check auth */
  NULL,				/* Check Access */
  NULL,				/* type checker */
  NULL,				/* Fixups */
  NULL,				/* logger */
  NULL,				/* Header Parser */
  NULL,				/* child init */
  NULL,				/* child exit */
  NULL,				/* post read request */
};
  
