#+cmu (ext:file-comment "$Id$")
;;
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/

;; tests for the clhp package. Uses cl-xptest testing package

(in-package :user)
(eval-when (:load-toplevel :compile-toplevel)
  (require :xptest))

(defpackage #:net.common-lisp.aventimiglia.clhp.xptests
  (:nicknames #:clhp.xptests)
  (:use #:common-lisp #:xptest)
  (:export #:check #:string-test-suite))

(in-package #:net.common-lisp.aventimiglia.clhp.xptests)

(defmacro with-test ((&rest bindings) predicate &rest fail-format)
  `(let (,@bindings)
     (unless ,predicate (failure ,@fail-format))))

;; test-fixture for string functions -- I put the expected results here,
;; so they can all be set together in SETUP
(def-test-fixture string-fixture ()
  ((string :accessor test-string :type string)
   (exploded :accessor exploded-string :type list) ; result of explode-string
   (split-char :accessor split-char :type character)
   (split-result :accessor split-result :type list))
  (:documentation "Test fixture for clhp string functions"))


(defmethod setup ((fix string-fixture))
  (setf (test-string fix) "Hello World"
	(exploded-string fix) (list #\H #\e #\l #\l #\o #\Space
				    #\W #\o #\r #\l #\d)
	(split-char fix) #\Space
	(split-result fix) '((#\H #\e #\l #\l #\o)
			     (#\W #\o #\r #\l #\d))))

(defmethod teardown ((fix string-fixture)) t)

(defmethod explode-string-1 ((test string-fixture))
  (with-test ((result (clhp::explode-string #1=(test-string test))))
	     (equal result #2=(exploded-string test))
	     "(CLHP::EXPLODE-STRING ~S) did not equal ~S~%" #1# #2#))

(defmethod split-char-list-1 ((test string-fixture))
  (with-test ((res (clhp::split-char-list #1=(split-char test)
                                          #2=(exploded-string test))))
             (equal res #3=(split-result test))
             "(CLHP::SPLIT-CHAR-LIST ~S ~S) did not evaluate to ~S~%"
             #1# #2# #3#))

(defmethod implode-string-1 ((test string-fixture))
  (with-test ((res (clhp::implode-string #2=(exploded-string test))))
	     (string= res #3=(test-string test))
	     "(CLHP::IMPLODE-STRING ~S) did not evaluate to ~S.~%" #2# #3#))
	     

(defparameter string-test-suite
  (make-test-suite "CLHP String Test Suite"
		   "Test suite to test string operations in :CLHP."
		   ("explode-string test 1" 'string-fixture
		    :test-thunk 'explode-string-1
		    :description "A test for CLHP::EXPLODE-STRING")
		   ("split-char-list test 1" 'string-fixture
		    :test-thunk 'split-char-list-1
		    :description "A test for CLHP::SPLIT-CHAR-LIST")
		   ("implode-string test 1" 'string-fixture
		    :test-thunk 'implode-string-1
		    :description "Test for CLHP::IMPLODE-STRING")))


(defun check ()
  (report-result (run-test string-test-suite) :verbose t))


