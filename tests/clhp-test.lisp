#+cmu (ext:file-comment "$Id$")
;;
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/

;; Test suite to for the :CLHP package.  The aim of this is to test all
;; external, and possibly internal symbols in the package
(eval-when (:load-toplevel :compile-toplevel)
  (unless (find-package :cgi)
    (load "../cgi"))
  (unless (find-package :clhp)
    (load "../clhp"))
  (unless (find-package :net.common-lisp.aventimiglia.test-suite)
    (load "test-suite")))

(use-package :test-suite)

;; This is used to import internal symbols for some of the macro
;; tests. First we unintern, just to make sure there are no name
;; clashes. You should pass the full name of the symbol to intern,
;; such as clhp::*pi-start*
(defmacro import-func (s)
  `#'(lambda () (unintern ',s) (import ',s)))

;; Used to reverse the above
(defmacro unintern-func (s)
  `#'(lambda () (unintern ',s)))

(defparameter *PI-TEST-STRING*
  "<tag>starts <?clhp here and ends ?> there <tag>")
(defparameter  *test-file*
  "<start>Hello There <?clhp (echo \"All of You\") ?></start>")
(defparameter *test-file-name* "parse-test.clhp")

(defparameter *test-file-include-output*
  "<start>Hello There All of You</start>")

(defparameter *test-file-parse-output*
  (concatenate 'string "X-Powered-By: CLHP/0.1.1alpha Common Lisp Hypertext Preprocessor
Content-type: TEXT/HTML

" *test-file-include-output*))

(defun make-test-file ()
  (with-open-file
   (f *test-file-name*
      :direction :output
      :if-exists :overwrite
      :if-does-not-exist :create)
   (format f *test-file*)))

(defun delete-test-file ()
  (delete-file *test-file-name*))

;  #:echo #:include #:xml-element
;  #:make-xml-element #:copy-xml-element #:xml-element-attributes
;  #:xml-element-name #:xml-element-contents #:xml-element-p #:tag
(defparameter *clhp-tests*
      (list
       ;; I know it's redundant to keep importing the same symbol, but
       ;; all these tests ahould stand on their own
       (make-instance 'function-test-data
		      :symbol 'clhp::find-pi-start
		      :pre-function (import-func clhp::*pi-start*)
		      :post-function (unintern-func clhp::*pi-start*)
		      :test-args `(,*pi-test-string*)
		      :result-form 12)
       (make-instance 'function-test-data
		      :symbol 'clhp::find-pi-start
		      :pre-function (import-func clhp::*pi-start*)
		      :post-function (unintern-func clhp::*pi-start*)
		      :test-args `(,*pi-test-string* :start 14)
		      :result-form nil)
       (make-instance 'function-test-data
		      :symbol 'clhp::find-pi-start
		      :pre-function (import-func clhp::*pi-start*)
		      :post-function (unintern-func clhp::*pi-start*)
		      :test-args `(,*pi-test-string* :end 8)
		      :result-form nil)
       (make-instance 'function-test-data
		      :symbol 'clhp::find-pi-end
		      :pre-function (import-func clhp::*pi-end*)
		      :post-function (unintern-func clhp::*pi-end*)
		      :test-args `(,*pi-test-string*)
		      :result-form 33)
       (make-instance 'function-test-data
		      :symbol 'clhp::find-pi-end
		      :pre-function (import-func clhp::*pi-end*)
		      :post-function (unintern-func clhp::*pi-end*)
		      :test-args `(,*pi-test-string* :start 34)
		      :result-form nil)
       (make-instance 'function-test-data
		      :symbol 'clhp::find-pi-end
		      :pre-function (import-func clhp::*pi-end*)
		      :post-function (unintern-func clhp::*pi-end*)
		      :test-args `(,*pi-test-string* :end 31)
		      :result-form nil)
       (make-instance 'output-function-test-data
		      :symbol 'clhp:parse
		      :pre-function #'make-test-file
		      :post-function #'delete-test-file
		      :test-args `(,*test-file-name*)
		      :output *test-file-parse-output*
		      :result-form *test-file*)
      (make-instance 'output-function-test-data
                     :symbol 'clhp:include
                     :pre-function #'make-test-file
                     :post-function #'delete-test-file
		     :test-args `(,*test-file-name*)
		     :output *test-file-include-output*
		     :result-form *test-file*)
      ;; PARSE-BUFFER is the core, it should be tested quite thuroughly
      (make-instance 'output-function-test-data
		     :symbol 'clhp::parse-buffer
		     :test-args '("Hello <?clhp (clhp:echo 'there) ?>" :end 34)
		     :output "Hello THERE")
      ;; This actually should be tested for all the errors it could produce
      (make-instance 'output-function-test-data
                     :symbol 'clhp::evaluate-code-block
		     :test-args '("(princ (list 1 2 3 4 5 6))")
		     :result-form nil
		     :output "(1 2 3 4 5 6)")
      (make-instance 'output-function-test-data
		     :symbol 'clhp:echo
		     :test-args '("One" 'two "Three")
		     :result-form nil
		     :output "OneTWOThree")
      (make-instance 'output-function-test-data
		     :symbol 'clhp:echo
		     :test-args '('#:|Four| (+ 3 2) (* 2 3))
		     :output "Four56"
		     :result-form nil)
      ))


(eval-when (load)
  (unix:unix-exit
   (cadr (multiple-value-list (run-tests *clhp-tests*)))
   )
  )


