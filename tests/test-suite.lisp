#+cmu (ext:file-comment "$Id$")
;;
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/

;; This is a set of test-suite classes that will be used to test the
;; package during development.

(defpackage #:net.common-lisp.aventimiglia.test-suite
  (:nicknames #:test-suite)
  (:use :cl)
  (:export #:run-tests #:function-test-data
	   #:output-function-test-data #:side-effect-function-test-data
	   #:output-function-test-data-output ))
(in-package :test-suite)

;; These macros Used for run-test methods
(defmacro call-if-function (form)
  `(when (functionp ,form) (funcall ,form)))

(defmacro test-return (test &rest args)
  `(cons (test-data-symbol data)
    (if ,test
	(progn (princ 'ok stream) (terpri) :OK)
      (progn (princ 'failed stream) (format stream ,@args)
	     (terpri) :FAILED))))

(defclass test-data ()
  ((symbol :initform NIL
	   :type symbol
	   :reader test-data-symbol
	   :initarg :symbol
	   :documentation "The symbol name to be tested")
   (pre-function :initform NIL
		 :type (or function nil)
		 :reader test-data-pre-function
		 :initarg :pre-function
		 :documentation "Function to be called prior to running tests")
   (post-function :initform NIL
		  :type (or function nil)
		  :reader test-data-post-function
		  :initarg :post-function
		  :documentation "Function to be called after running tests"))
  (:documentation "Abstract supertype for CLASS, STRUCTURE, VARIABLE
and FUNCTION test-data"))

(defclass function-test-data (test-data)
  ((test-args :initform NIL
	      :type list
	      :reader function-test-data-test-args
	      :initarg :test-args
	      :documentation "A list of arguments to be passed to the
function for testing")
   (result-form :initform NIL
		:reader function-test-data-result-form
		:initarg :result-form
		:documentation "The expected return value when SYMBOL
is called with TEST-ARGS"))
  (:documentation "A class to test functions or macros, taking
TEST-ARGS as a list of arguments to call the function with and
expecting RESULT-FORM to be the result"))
	
(defclass output-function-test-data (function-test-data)
  ((output :initform NIL
	   :type string
	   :reader output-function-test-data-output
	   :initarg :output
	   :documentation "A string which should be equal to the output"))
  (:documentation "A subclass of function-test-data for testing macros
or functions, but this is used when thye output to *STANDARD-OUTPUT*
must be tested as well."))

(defclass side-effect-function-test-data (function-test-data)
					  
  ((var-list :initform NIL
	     :type list
	     :reader side-effect-function-test-data-var-list
	     :initarg :var-list
	     :documentation "An a-list of ((SYMBOL VALUE)) pairs. All
Symbols should be EQUAL to the VALUES after test function is
evaluated."))
  (:documentation "A subclass of function-test-data used to test
functions which have side effects of setting global variables."))

;; It's important that the RUN-TEST methods below all use DATA as the
;; TEST-DATA object name, because some of the macros defined at the
;; top of the file are hard coded to use the common names.


(defmethod run-test ((data function-test-data)
		     &optional (stream *standard-output*))
  (unwind-protect
      (progn
	(call-if-function (test-data-pre-function data))
	(let* ((test-form (cons (test-data-symbol data)
				(function-test-data-test-args data)))
	       (result (eval test-form)))
	  (format stream "~S --> ~S : " test-form result)
          (let ((test-result (function-test-data-result-form data)))
            (test-return (equal result test-result)
                         "~S expected" test-result))))
    (call-if-function (test-data-post-function data))))
    
(defmethod run-test ((data output-function-test-data)
		     &optional (stream *standard-output*))
  (unwind-protect
      (progn
	(call-if-function (test-data-pre-function data))
	(let* ((test-form (cons (test-data-symbol data)
				(function-test-data-test-args data)))
	       (output (make-array 0 :element-type 'base-char
				   :fill-pointer 0 :adjustable t))
	       (result (with-output-to-string
			 (*standard-output* output)
			 (eval test-form))))
	  (format stream "~S --> ~S ~S : " test-form output result)
	  (let ((test-output (output-function-test-data-output data))
		(test-result (function-test-data-result-form data)))
	    (test-return (and (equal result test-result)
			      (string= output test-output))
			 "~S -> ~S expected" test-output test-result))))
	(call-if-function (test-data-post-function data))))

(defmethod run-test ((data side-effect-function-test-data)
		     &optional stream)
  (unwind-protect
      (progn
	(call-if-function (test-data-pre-function data))
	(let* ((test-form (cons (test-data-symbol data)
				(function-test-data-test-args data)))
	       (result (eval test-form))
	       (test-var-list (side-effect-function-test-data-var-list
			       data))
	       (vars (mapcar #'(lambda (c) (car c)) test-var-list))
	       (var-list (mapcar
			  #'(lambda (v) (list v (eval v))) vars)))
	  (format stream "~S --> ~S ~S : "
		  test-form result var-list)
	  (let ((test-result (function-test-data-result-form data)))
	    (test-return (and (equal result test-result)
			      (equal test-var-list var-list))
			 "~S and ~S expected" test-result test-var-list))))
    (call-if-function (test-data-post-function data))))
    
;; Example
;(defparameter list-test (make-instance 'function-test-data
;                                :symbol 'list
;                                :test-args '(1 2 3 4 5)
;                                :result-form '(1 2 3 4 5)))
;
;* (run-test list-test)
;> (LIST 1 2 3 4 5) --> (1 2 3 4 5) : OK
;> (LIST . :OK )  

(defun run-tests (test-list)
  "Given a list of test-objects, run each test, and report
results. Returns (VALUES PASS FAIL) where PASS is the number of tests
passed and FAIL is the number of Failed tests"
  (let ((pass 0)
	(fail 0))
    (dolist (test test-list)
      (format t "~%------------------ Testing ~A~%"
	      (test-data-symbol test))
      (if (eq :OK (cdr (run-test test)))
	  (incf pass)
	(incf fail)))
    (format t "~%-------------------- Testing finished~%")
    (format t "~8T~A Tests performed~%~8T~A Passed ~%~8T~A Failed~%~%"
	    (+ pass fail) pass fail)
    (values pass fail)))
