#+cmu (ext:file-comment "$Id$")
;;
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/

;; Test suite to for the :CGI package.  The aim of this is to test all
;; external, and possibly internal symbols in the package

(eval-when (:load-toplevel :compile-toplevel)
  (unless (find-package :cgi)
    (load "../cgi"))
  (unless (find-package :net.common-lisp.aventimiglia.test-suite)
    (load "test-suite"))
  (when (find-package :clhp) (delete-package :clhp)))

(use-package :test-suite)

;; Still to be tested
;; Functions which have side effects and no return values
;; INIT
;;
;; And
;; POST-DATA  HANDLE-GENERAL-ERROR

;;  Use the following to run-tests
;; (mapcar #'run-test *cgi-tests*)
(defparameter *cgi-tests*
  (list
;   (make-instance 'function-test-data
;                  :symbol 'cgi::explode-string
;                  :test-args '("Hello There")
;                  :result-form '(#\H #\e #\l #\l #\o #\Space #\T #\h #\e #\r
;                                 #\e))
;   (make-instance 'function-test-data
;                  :symbol 'cgi::split-char-list
;                  :test-args '(#\Space (cgi::explode-string "Ab CdE F"))
;                  :result-form '((#\A #\b) (#\C #\d #\E) (#\F)))
;   (make-instance 'function-test-data
;                  :symbol 'cgi::implode-string
;                  :test-args '((cgi::explode-string "Hello"))
;                  :result-form "Hello")
   (make-instance 'function-test-data
		  :symbol 'cgi::url-decode-char-list
		  :test-args '((cgi::explode-string "foo%34%2a"))
		  :result-form '(#\f #\o #\o #\4 #\*))
   (make-instance 'function-test-data
		  :symbol 'cgi::make-keyword
		  :test-args '("Hello")
		  :result-form :hello)
   (make-instance 'function-test-data
		  :symbol 'cgi::read-n-chars
		  :test-args '(4 (make-string-input-stream "Hello"))
		  :result-form '(#\H #\e #\l #\l))
   (make-instance 'function-test-data
		  :symbol 'cgi::get-data
		  :pre-function #'(lambda ()
				    (let ((a (car (assoc :query_string
						    cgi::*server-env*))))
				      (when a
					(setf cgi:*server-env*
					      (remove a cgi:*server-env*))))
				    (push (list :query_string
						"index=foo&type=bar%20baz")
					  cgi:*server-env*))
		  :post-function #'(lambda () (setq cgi:*server-env* nil))
		  :test-args nil
		  :result-form '(#\i #\n #\d #\e #\x #\= #\f #\o #\o #\& #\t
				 #\y #\p #\e #\= #\b #\a #\r #\% #\2 #\0 #\b
				 #\a #\z))
   (make-instance 'function-test-data
		  :symbol 'cgi::a-list-value
		  :test-args '(2 '((1 f) (3 g) (6 h) (2 y)))
		  :result-form 'y)
   (make-instance 'output-function-test-data
		  :symbol 'cgi:debug
		  :test-args '('(list 1 2 3))
		  :output (format nil
				  "(CGI:DEBUG: (LIST 1 2 3) --> (1 2 3))~%"))
   (make-instance 'function-test-data
		  :symbol 'cgi::ca-list-to-a-list
		  :test-args '('((a . 1)(b . 2)(c . 3)))
		  :result-form '((a 1)(b 2)(c 3)))
   (make-instance 'function-test-data
		  :symbol 'cgi::list-to-a-list
		  :test-args '('(1 2 3 4 5 6 7 8))
		  :result-form '((1 2)(3 4)(5 6)(7 8)))
   (make-instance 'function-test-data
		  :symbol 'cgi::list-to-a-list
		  :test-args '('(1 2 3 4 5 6 7 8 9))
		  ;; This should be (values ((1 2) .. ) 9) the testing
		  ;; system hgas to be refined
		  :result-form '((1 2)(3 4)(5 6)(7 8)))
   (make-instance 'function-test-data
		  :symbol 'cgi::query-to-a-list
		  :test-args '((cgi::explode-string
			       "Hello=there&no=yes&red=blue"))
		  :result-form '(("Hello" "there") ("no" "yes")
				 ("red" "blue")))
;   (make-instance 'side-effect-function-test-data
;                  :symbol 'cgi:init
;                  :pre-function #'(lambda ()
;                                    (setq ext:*environment-list*
;                                          '((:request_method . "get")
;                                            (:query_string . "hi=4&a=5"))))
;                  :post-function #'(lambda ()
;                                     (setq ext:*environment-list* nil))    
;                  :result-form '(values)
;                  :var-list '((cgi:*server-env* t)))
   (make-instance 'output-function-test-data
		  :symbol 'cgi:header
		  :output (format nil
				  "Content-type: TEXT/PLAIN~%~%")
		  :result-form t)
   ;; We test header twice to make sure it only outputs the first
   ;; time. The post-function should reset the internals of the header
   ;; function so successive tests will pass, but by reloading the
   ;; package, all the symbols in this list get uninterned, however
   ;; they are still bound to the functions. so in order to run the
   ;; tests again, you have to re evaluate this setq. This is only a
   ;; problem in interactive env. if these tests are being run as a
   ;; one time deal (which is the eventual goal) none of this will be
   ;; a problem.
   (make-instance 'output-function-test-data
		  :post-function #'(lambda ()
				     (delete-package :cgi)
				     (fmakunbound 'cgi:header)
				     (load "library:cgi"))
		  :symbol 'cgi:header
		  :output "")
   ))


(eval-when (load)
  (unix:unix-exit (cadr (multiple-value-list (run-tests *cgi-tests*)))))

      
      