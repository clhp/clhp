#+cmu (ext:file-comment "$Id: package.lisp,v 1.3 2003/11/12 22:41:55 aventimiglia Exp $")
;;
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/

(in-package #:cl-user)

(defpackage #:net.common-lisp.aventimiglia.clhp
  (:nicknames #:clhp #:cgi)		; The CGI will go away eventually
  (:use #:cl #:ext)
  (:export #:*server-env* #:*query-vars* #:debug #:init
	   #:parse #:*clhp-version* #:echo #:xml-element
	   #:make-xml-element #:copy-xml-element #:xml-element-attributes
	   #:xml-element-name #:xml-element-contents #:xml-element-p #:tag
	   #:require
	   ))