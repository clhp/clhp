#+cmu (ext:file-comment "$Id: cgi.lisp,v 1.14 2003/10/21 16:57:27 aventimiglia Exp $")
;;
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/

;; This is the "Makefile" for clhp, to build and install, be inside
;; the clhp directory, start cmucl and (LOAD "build")
;;
;; 

(in-package :cl-user)

(defpackage #:net.common-lisp.aventimiglia.clhp.build
  (:nicknames #:build)
  (:use #:cl))

(in-package :build)

;; There are 3 options for the use of this program,
;;
;; 1. configure
;; 2. make
;; 3. install
;;
;; each successive option depends on the first being completed. Once
;; configuration is done, we will save the configuration in case the
;; program has to be re-run

;; -- First check for asdf

;; Installation locations, we'll try to figure it out ourselves, but
;; user will be asked for specifics.

;; -- Source files will be installed in asdf:*central-registry*

;; -- core will be installed in cmucl lib directory 
