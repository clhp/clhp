#+cmu (ext:file-comment "$Id: cgi.lisp,v 1.11 2003/10/17 12:58:04 aventimiglia Exp $")
;;
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/

(in-package :clhp)

(defmacro cond-bind ((&rest bindings) &body body)
  "A COND wrapped in a Let"
  `(let (,@bindings) (cond ,@body)))

(defmacro if-bind ((&rest bindings) test if else)
  "An IF wrapped in a LET"
  `(let (,@bindings) (if ,test ,if ,else)))

(defmacro a-list-value (key a-list)
  "returns the value from a (KEY . VALUE) A-LIST"
  `(cadr (assoc ,key ,a-list)))

(defun ca-list-to-a-list (list)
  "Converts a CONS type a-list '((A . 3)(B . 4)) to a list type '((A 3)(B 4))"
  (mapcar #'(lambda (cons) (list (car cons)(cdr cons))) list))

(defmacro make-keyword (name)
  "Translates a string into a keyword: (MAKE-KEYWORD \"foo\") -->
:FOO"
  `(read-from-string (format nil ":~a" ,name)))

(defun list-to-a-list (list &optional a-list)
  "Converts a list to an a-list, pairing odd and even elements. If an
odd number of elements are in LIST, the last element is returnes as
the second value.
ex: (LIST-TO-A-LIST '(a b c d) --> '((a b)(c d)) NIL
ex: (LIST-TO-A-LIST '(1 2 3 4 5) --> '((1 2)(3 4)) 5"
  (cond
   ((null list) (nreverse a-list))
   ((= 1 (length list)) (values (nreverse a-list) (car list)))
   (t (list-to-a-list (cddr list)
		      (cons (list (car list) (cadr list))
			    a-list)))))


