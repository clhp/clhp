(ext:file-comment
 "$Id: clhp.lisp,v 1.22 2003/11/14 22:32:10 aventimiglia Exp $")
;;
;; CLHP the Common Lisp Hypertext Preprocessor
;; (C) 2003 Anthony J Ventimiglia
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 2.1 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;;
;; email: aventimiglia@common-lisp.net
;; HomePage: http://common-lisp.net/project/clhp/

(in-package :clhp)

;; This elaborate reader macro converts the cvs Date keywords and
;; translates it into a 8 digit date code for marking the cvs version.
;; by changing the NIL in the first part of the OR, a release number
;; will override the CVS keyword
(defconstant *CLHP-VERSION*
  #.(or nil				; Set this for releases
	(let* ((trimmed (string-trim "$ Date:" "$Date: 2003/11/14 22:32:10 $"))
	       (date (subseq trimmed 0 (search " " trimmed))))
	  (concatenate 'string
		       (subseq date 0 4)
		       (subseq date 5 7)
		       (subseq date 8 10)
		       "cvs"))))
(defconstant *PI-START* "<?clhp")
(defconstant *PI-END* "?>")

;; The Xml-Element Structure, with its print function makes generation and
;; printing of generic XML elements quite lovely
(defstruct (xml-element (:print-function pprint-xml-element))
  "An XML element structure. NAME can be a symbol or string and
reflects the element name. ATTRIBUTES is an a-list of (NAME VALUE)
pairs corresponding to the elements Attributes. CONTENTS should
eveluate to a string, symbol, tag or list of strings, symbols or tags,
which may be mixed. Using the PPRINT-XML-ELEMENT function, these tags are
printed as they will appear."
  (name NIL :type (or symbol string) :read-only t)
  (attributes NIL :type list)
  (contents NIL :type (or string symbol cons xml-element)))

(defmacro find-pi-start (buffer &key (start 0) end)
  "Find the next occurence of *PI-START* and return its index. Returns
NIL if not found"
  (declare (type (array character 1) buffer)
	   (type fixnum start))
  `(search ,*PI-START* ,buffer :test #'string= :start2 ,start :end2 ,end))

(defmacro find-pi-end (buffer &key (start 0) end)
  (declare (type (array character 1) buffer)
	   (type fixnum start end))	       
  "Find the next occurence of *pi-end* and return its index. Returns
NIL if not found."
  `(search ,*PI-END* ,buffer :test #'string= :start2 ,start :end2 ,end))

(defun parse (file-name)
  "This is the only thing that needs to be called from a CL-CGI script
to use CLHP. See README for an example."
  (use-package :cgi)
  (use-package :clhp)
  (init)
  ;; We'll read the whole thing here into a buffer, then send it to
  ;; PARSE-BUFFER to recursively process it
  (handler-bind
   ((error #'handle-error))
   (with-open-file
    (stream file-name :direction :input :if-does-not-exist :error)
    ;; FILE-LENGTH could return NIL, I don't see why, but nevertheless
    ;; I will have to write an error checking macro around it to
    ;; signal an error if file-length can't be determined.
    (let* ((buffer-size (file-length stream))
	   (buffer (make-array buffer-size :element-type 'character)))
      ;; Should also check for anything fishy like a read that does not
      ;; match FILE-SIZE
      (read-sequence buffer stream)
      (parse-buffer buffer :end buffer-size)))))

  
(defun parse-buffer (buffer &key (start 0) end (code-block nil) (in-block nil))
  "Takes blocks of text passed from PARSE, evaluates anything inside
the <?clhp ?> elements, and dumps the rest through unscathed."
  (declare (type (array character 1) buffer)
	   (type fixnum end))
  (cond-bind  ((index (if in-block
			  (find-pi-end buffer :start start :end end)
			(find-pi-start buffer :start start :end end))))
     ((>= start end) 			; Done with this buffer
      nil)
     ((and in-block index) 		; Found the end of a code-block
      (evaluate-code-block
       (append code-block (coerce (subseq buffer start index) 'list)))
      (parse-buffer buffer 
		    :start (+ index #.(length *PI-END*))
		    :end end))
     ((and (not in-block) index)	; Found code-block start
      (write-sequence buffer *standard-output* :start start :end index)
      (parse-buffer buffer
		    :start (+ index #.(length *PI-START*))
		    :end end
		    :in-block t))
     (in-block (signal 'parse-error))
     (t 				; Not in code-block no start in sight
      (write-sequence buffer *standard-output* :start start :end end))))

(defun evaluate-code-block (code-block)
  "Read the Lisp object represented by CODE-BLOCK, and evaluate it."
  (declare (type list code-block))
  (let ((form nil)
	(index 0)
	(eof (gensym)))
    (loop
     (handler-case
      (progn
	(multiple-value-setq
	    (form index)
	  (read-from-string (coerce code-block 'string)
			    nil eof :start index))
	  (when (eq eof form) (return))
	  (eval form))
      (condition (c)
		 (echo
		  (tag "p"
		       (tag "i"
			    (list
			     (format nil "~&CLHP: ~A in the form ~A : "
				     (type-of c) form)
			     (report-error-string c)))))
		 (when (typep c 'end-of-file) (return)))))))
		 


(defun echo (string &rest more)
  "This allows long strings or lisp objects to be broken up into
separate lines as separate strings, all the strings passed will be
concatenated and printed to *STANDARD-OUTPUT*. It is analogous to the
way string constants are all concatenated by the C++ compiler, or the
way strings can be concatenated with the '.' operator in PHP. There is
no method for newlines, use the standars TERPRI or FRESH-LINE
functions."
  (dolist (chunk (cons string more)) (princ chunk)))

(defun pprint-xml-element (xml-element stream depth)
 (declare (ignore depth))
 (let ((name (xml-element-name xml-element))
       (contents (xml-element-contents xml-element)))
   (format stream "<~A~:{ ~A=\"~A\"~}>~:[~A~;~{~A~}~]</~A>"
           name
           (xml-element-attributes xml-element)
           (listp contents)
           contents
           name)))

;; This is a convenience function for MAKE-XML-ELEMENT
(defun tag (&rest args)
  "Creates an XML-ELEMENT, where (CAR ARGS) fills the :NAME slot. If
ARGS has an even number of elements, then (CDR (BUTLAST ARGS)) is
converted to an a-list to fill the :ATTRIBUTES slot, and (CAR (LAST
ARGS)) fills :CONTENTS, otherwise (CDR ARGS) is converted to an a-list
for :ATTRIBUTES, and :CONTENTS is NIL
ex: (tag 'A 'HREF \"http://bogus.com/\" \"Simple Link\")
          --> <A HREF=\"http://bogus.com/\">Simple Link</A>
    (tag 'img 'src \"pic.png\")
          --> <IMG SRC=\"pic.png\"></IMG>"
  (multiple-value-bind
      (att-list contents)
      (list-to-a-list (cdr args))
    (make-xml-element :name (car args)
	      :attributes att-list
	      :contents contents)))

;; Similar to PHP's require, loads a lisp file in the local directory.
(defun require (filename)
  "Load the lisp source or fasl file FILENAME, relative to the document root"
  (let ((doc-root
	 (make-pathname :name nil :type nil :version nil
			:defaults (parse-namestring
				   (gethash :script_filename *server-env*)))))
    (load (merge-pathnames doc-root filename))))

;; Error handling is probably the biggest room to work here.  I should
;; eventually make a handler that binds the evaluation of clhp PI
;; embedded code, that handler could then relay the entire block of
;; code in its error reporting.
(defun handle-error (condition)
  "Top level generic error"
  (let ((error-type (type-of condition)))
    (format t "Error: ~a : ~a"
	    error-type (report-error-string condition))))

(defun report-error-string (condition)
  "Given a condition, returns a string describing the error. This is
used by the error-handler in EVALUATE-CODE-BLOCK and HANDLE-ERROR."
  (format nil "~A"
	  (case (type-of condition)
	    ((or unbound-variable undefined-function)
	     (cell-error-name condition))
	    (end-of-file
	     (stream-error-stream condition))
	    (kernel:simple-file-error
	     (file-error-pathname condition))
	    (otherwise
	     "Add a handler in REPORT-ERROR-STRING for this condition"))))